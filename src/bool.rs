mod sealed {
    pub trait Sealed<T> {}
}

pub trait Equals<T>: sealed::Sealed<T> {}

impl<T> sealed::Sealed<T> for T {}
impl<T> Equals<T> for T {}
