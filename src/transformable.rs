use std::convert::Infallible;

pub use typestuff_macro::transformable;

pub trait Transformable: 'static {
    const FIELDS: &'static [&'static dyn Field<Object = Self>];
}

pub trait Transformation {
    type Map<F, T>
    where
        F: Field;
}

pub trait Transformer<Source, Destination>
where
    Source: Transformation,
    Destination: Transformation,
{
    type Error;

    fn map<F, T>(
        &mut self,
        value: Source::Map<F, T>,
    ) -> Result<Destination::Map<F, T>, Self::Error>
    where
        F: Field;
}

pub trait Field {
    type Object: Transformable;

    fn get() -> Self
    where
        Self: Sized;

    fn name(&self) -> &'static str;
}

impl Transformation for () {
    type Map<F, T> = T
    where
        F: Field;
}

impl<Tr> Transformer<Tr, Tr> for ()
where
    Tr: Transformation,
{
    type Error = Infallible;

    fn map<F, T>(&mut self, value: Tr::Map<F, T>) -> Result<Tr::Map<F, T>, Self::Error>
    where
        F: Field,
    {
        Ok(value)
    }
}
