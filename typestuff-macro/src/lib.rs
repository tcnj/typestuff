use proc_macro::TokenStream;
use proc_macro2::{Ident, TokenStream as TokenStream2};
use quote::{format_ident, quote, quote_spanned};
use syn::{parse_macro_input, spanned::Spanned, DeriveInput, ItemStruct, LitStr, Type};

#[proc_macro_attribute]
pub fn transformable(attr: TokenStream, input: TokenStream) -> TokenStream {
    let attr = TokenStream2::from(attr);
    let input = TokenStream2::from(input);
    let span = input.span();

    let expanded = match transformable_impl(attr, input) {
        Ok(ts) => ts.into(),
        Err(e) => {
            let expanded = quote_spanned! { span =>
                compile_error!(#e);
            };

            expanded.into()
        }
    };

    // println!("{}", expanded);

    expanded
}

fn transformable_impl(_attr: TokenStream2, input: TokenStream2) -> Result<TokenStream2, String> {
    let input = match syn::parse2::<ItemStruct>(input) {
        Ok(input) => input,
        Err(e) => return Ok(e.into_compile_error().into()),
    };

    let vis = input.vis;
    let ident = input.ident;

    let fields = match input.fields {
        syn::Fields::Named(fields) => fields.named,
        _ => return Err("Only named fields are supported".into()),
    };

    let fields_mod_ident = format_ident!("{}_fields", ident);

    let field_structs = {
        let fields = fields.iter().map(|f| {
            let field_ident = f.ident.as_ref().unwrap();
            let vis = match vis {
                syn::Visibility::Public(_) => quote! {pub},
                syn::Visibility::Restricted(_) | syn::Visibility::Crate(_) => quote! {pub(crate)},
                syn::Visibility::Inherited => quote! {pub(in super)},
            };
            let name_lit = LitStr::new(&field_ident.to_string(), field_ident.span());
            quote! {
                #[allow(non_camel_case_types)]
                #vis struct #field_ident;
                impl ::typestuff::transformable::Field for #field_ident {
                    type Object = super::#ident<()>;

                    fn get() -> Self
                    where
                        Self: Sized
                    {
                        #field_ident
                    }

                    fn name(&self) -> &'static str {
                        #name_lit
                    }
                }
            }
        });
        quote! {
            #[allow(non_snake_case)]
            #vis mod #fields_mod_ident {
                #(#fields)*
            }
        }
    };

    let expanded = {
        let attrs = input.attrs;
        let kstruct = input.struct_token;

        let fields_const = fields.iter().map(|f| f.ident.as_ref().unwrap().clone());

        let fields = fields.iter().map(|f| {
            let mut f = f.clone();
            let ty = f.ty;
            let field_name = f.ident.as_ref().unwrap();
            f.ty = Type::Verbatim(quote! {
                <T as ::typestuff::transformable::Transformation>::Map<#fields_mod_ident :: #field_name, #ty>,
            });
            f
        });

        quote! {
            #field_structs

            #(#attrs)*
            #vis #kstruct #ident <T=()> where T: ::typestuff::transformable::Transformation {
                #(
                    #fields
                )*
            }

            impl ::typestuff::transformable::Transformable for #ident<()> {
                const FIELDS: &'static [&'static dyn ::typestuff::transformable::Field<Object=Self>] = &[
                    #(&#fields_mod_ident::#fields_const),*
                ];
            }
        }
    };

    Ok(expanded)
}

#[proc_macro_derive(TypeFn)]
pub fn derive_type_fn(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    struct TypeFnFrame {
        marker_name: Ident,
        result_name: Ident,
        types: Vec<Ident>,
    }

    let mut type_acc = input
        .generics
        .type_params()
        .map(|param| param.ident.clone())
        .collect::<Vec<_>>();
    type_acc
        .pop()
        .expect("At least one type parameter required");

    let mut next_result_name = input.ident.clone();
    let frames = (0..=(type_acc.len()))
        .map(|i| {
            let marker_name = format_ident!("{}{}", input.ident, "_".repeat(i + 1));

            let result_name = std::mem::replace(&mut next_result_name, marker_name.clone());

            let types = type_acc.clone();
            type_acc.pop();

            TypeFnFrame {
                marker_name,
                result_name,
                types,
            }
        })
        .map(
            |TypeFnFrame {
                 marker_name,
                 result_name,
                 types,
             }| {
                quote! {
                    pub struct #marker_name<#(#types),*> {
                        _phantom: ::std::marker::PhantomData<(#(*const #types,)*)>
                    }

                    impl<#(#types),*> ::typestuff::type_fn::TypeFn for #marker_name<#(#types),*> {
                        type Apply<A> = #result_name<#(#types),* A>;
                    }

                    impl<#(#types,)* A> ::typestuff::type_fn::TypeMatch for #result_name<#(#types,)* A> {
                        type Constructor = #marker_name<#(#types,)*>;
                        type Argument = A;
                    }
                }
            },
        );

    let expanded = quote! {#(#frames)*};

    println!("{}", expanded);

    expanded.into()
}
