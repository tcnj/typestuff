use std::collections::BTreeMap;
use std::marker::PhantomData;

use crate::bool::Equals;

pub use typestuff_macro::TypeFn;

pub trait TypeFn {
    type Apply<A>;
}

pub type Reapply<T, A> = <<T as TypeMatch>::Constructor as TypeFn>::Apply<A>;

pub trait TypeMatch: Equals<Reapply<Self, Self::Argument>> {
    type Constructor: TypeFn;
    type Argument;
}

macro_rules! impl_builtin {
    (
        $(
            $base_type:ident
            $symbol0:ident $marker0:ident
            $($($symbol:ident $marker:ident)+)?
        ),+ $(,)?
    ) => {
        $(
            pub struct $marker0$(<$($symbol ,)+>)? {
                $(
                    _phantom: PhantomData<($(*const $symbol,)+)>
                )?
            }

            impl$(<$($symbol ,)+>)? TypeFn for $marker0$(<$($symbol ,)+>)? {
                type Apply<A> = $base_type<$($($symbol ,)+)? A>;
            }

            impl<$symbol0 $($(, $symbol)+)?> TypeMatch for $base_type<$($($symbol,)+)? $symbol0> {
                type Constructor = $marker0$(<$($symbol ,)+>)?;
                type Argument = $symbol0;
            }

            $(
                impl_builtin!(
                    $marker0
                    $($symbol $marker)+
                );
            )?
        )+
    };
}

macro_rules! impl_builtin_backwards {
    (
        $(
            $base_type:ident
            $symbol0:ident $marker0:ident
            $($($symbol:ident $marker:ident)+)?
        ),+ $(,)?
    ) => {
        $(
            pub struct $marker0$(<$($symbol ,)+>)? {
                $(
                    _phantom: PhantomData<($(*const $symbol,)+)>
                )?
            }

            impl$(<$($symbol ,)+>)? TypeFn for $marker0$(<$($symbol ,)+>)? {
                type Apply<A> = $base_type<A $($(, $symbol)+)?>;
            }

            impl<$symbol0 $($(, $symbol)+)?> TypeMatch for $base_type<$symbol0 $($(, $symbol)+)?> {
                type Constructor = $marker0$(<$($symbol ,)+>)?;
                type Argument = $symbol0;
            }

            $(
                impl_builtin!(
                    $marker0
                    $($symbol $marker)+
                );
            )?
        )+
    };
}

impl_builtin!(
    Vec
    T0 Vec_,
    BTreeMap
    T0 BTreeMap_
    T1 BTreeMap__,
    Option
    T0 Option_
);

impl_builtin_backwards!(
    Result
    T Result_
    E Result__
);
