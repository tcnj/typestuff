pub mod applicative;
pub mod bool;
pub mod functor;
pub mod monad;
pub mod monad_fail;
pub mod transformable;
pub mod type_fn;
