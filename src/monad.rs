use crate::{
    applicative::{Applicative, ApplicativeMut, ApplicativeOnce},
    type_fn::Reapply,
};

// TRAITS

/// A monad is just a monoid in the category of endofunctors.
pub trait Monad: Applicative {
    fn and_then<U, F>(self, op: F) -> Reapply<Self, U>
    where
        F: Fn(Self::Argument) -> Reapply<Self, U>;

    fn sequence_over<U>(self, over: Reapply<Self, U>) -> Self;
}

pub trait MonadMut: ApplicativeMut {
    fn and_then_mut<U, F>(self, op: F) -> Reapply<Self, U>
    where
        F: FnMut(Self::Argument) -> Reapply<Self, U>;

    fn sequence_over_mut<U>(self, over: Reapply<Self, U>) -> Self;
}

impl<T> Monad for T
where
    T: MonadMut,
{
    fn and_then<U, F>(self, op: F) -> Reapply<Self, U>
    where
        F: Fn(Self::Argument) -> Reapply<Self, U>,
    {
        self.and_then_mut(op)
    }

    fn sequence_over<U>(self, over: Reapply<Self, U>) -> Self {
        self.sequence_over_mut(over)
    }
}

pub trait MonadOnce: ApplicativeOnce {
    fn and_then_once<U, F>(self, op: F) -> Reapply<Self, U>
    where
        F: FnOnce(Self::Argument) -> Reapply<Self, U>;

    fn sequence_over_once<U>(self, over: Reapply<Self, U>) -> Self;
}

impl<T> MonadMut for T
where
    T: MonadOnce,
{
    fn and_then_mut<U, F>(self, op: F) -> Reapply<Self, U>
    where
        F: FnMut(Self::Argument) -> Reapply<Self, U>,
    {
        self.and_then_once(op)
    }

    fn sequence_over_mut<U>(self, over: Reapply<Self, U>) -> Self {
        self.sequence_over_once(over)
    }
}

// IMPLS

impl<T> MonadOnce for Option<T> {
    fn and_then_once<U, F>(self, op: F) -> Reapply<Self, U>
    where
        F: FnOnce(Self::Argument) -> Reapply<Self, U>,
    {
        Option::and_then(self, op)
    }

    fn sequence_over_once<U>(self, over: Reapply<Self, U>) -> Self {
        let _ = over?;
        self
    }
}

impl<T> MonadMut for Vec<T>
where
    T: Clone,
{
    fn and_then_mut<U, F>(self, mut op: F) -> Reapply<Self, U>
    where
        F: FnMut(Self::Argument) -> Reapply<Self, U>,
    {
        self.into_iter().flat_map(|v| op(v).into_iter()).collect()
    }

    fn sequence_over_mut<U>(self, over: Reapply<Self, U>) -> Self {
        over.into_iter()
            .flat_map(|_| self.iter().cloned())
            .collect()
    }
}

impl<T, E> Monad for Result<T, E> {
    fn and_then<U, F>(self, op: F) -> Reapply<Self, U>
    where
        F: Fn(Self::Argument) -> Reapply<Self, U>,
    {
        Result::and_then(self, op)
    }

    fn sequence_over<U>(self, over: Reapply<Self, U>) -> Self {
        Result::and_then(over, |_| self)
    }
}
