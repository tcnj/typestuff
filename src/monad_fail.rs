use crate::monad::Monad;

pub trait MonadFail: Monad {
    fn fail(reason: String) -> Self;
}
