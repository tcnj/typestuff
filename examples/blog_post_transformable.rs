use typestuff::transformable::transformable;

#[transformable]
struct BlogPost {
    id: u32,
    title: String,
    content: String,
}

fn main() {}
