use crate::{functor::Functor, type_fn::Reapply};

// TRAITS

pub trait Applicative: Functor {
    fn pure(val: Self::Argument) -> Self;

    fn apply<F, B>(self, op: Reapply<Self, F>) -> Reapply<Self, B>
    where
        F: Fn(Self::Argument) -> B;
}

pub trait ApplicativeMut: Functor {
    fn pure_mut(val: Self::Argument) -> Self;

    fn apply_mut<F, B>(self, op: Reapply<Self, F>) -> Reapply<Self, B>
    where
        F: FnMut(Self::Argument) -> B;
}

impl<T> Applicative for T
where
    T: ApplicativeMut,
{
    fn pure(val: Self::Argument) -> Self {
        Self::pure_mut(val)
    }

    fn apply<F, B>(self, op: Reapply<Self, F>) -> Reapply<Self, B>
    where
        F: Fn(Self::Argument) -> B,
    {
        self.apply_mut(op)
    }
}

pub trait ApplicativeOnce: Functor {
    fn pure_once(val: Self::Argument) -> Self;

    fn apply_once<F, B>(self, op: Reapply<Self, F>) -> Reapply<Self, B>
    where
        F: FnOnce(Self::Argument) -> B;
}

impl<T> ApplicativeMut for T
where
    T: ApplicativeOnce,
{
    fn pure_mut(val: Self::Argument) -> Self {
        Self::pure_once(val)
    }

    fn apply_mut<F, B>(self, op: Reapply<Self, F>) -> Reapply<Self, B>
    where
        F: FnMut(Self::Argument) -> B,
    {
        self.apply_once(op)
    }
}

// IMPLS

impl<T> ApplicativeOnce for Option<T> {
    fn pure_once(val: Self::Argument) -> Self {
        Some(val)
    }

    fn apply_once<F, B>(self, op: Reapply<Self, F>) -> Reapply<Self, B>
    where
        F: FnOnce(Self::Argument) -> B,
    {
        let function = op?;
        let arg = self?;
        let result = function(arg);
        Some(result)
    }
}

impl<T> ApplicativeMut for Vec<T>
where
    T: Clone,
{
    fn pure_mut(val: Self::Argument) -> Self {
        vec![val]
    }

    fn apply_mut<F, B>(self, op: Reapply<Self, F>) -> Reapply<Self, B>
    where
        F: FnMut(Self::Argument) -> B,
    {
        op.into_iter()
            .flat_map(|mut f| self.iter().cloned().map(move |a| f(a)))
            .collect()
    }
}

impl<T, E> ApplicativeOnce for Result<T, E> {
    fn pure_once(val: Self::Argument) -> Self {
        Ok(val)
    }

    fn apply_once<F, B>(self, op: Reapply<Self, F>) -> Reapply<Self, B>
    where
        F: FnOnce(Self::Argument) -> B,
    {
        let function = op?;
        let arg = self?;
        let result = function(arg);
        Ok(result)
    }
}
