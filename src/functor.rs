use crate::type_fn::{Reapply, TypeMatch};

// TRAITS

pub trait Functor: TypeMatch {
    fn fmap<F, B>(self, mapper: F) -> Reapply<Self, B>
    where
        F: Fn(Self::Argument) -> B;

    fn replace<B>(self, with: B) -> Reapply<Self, B>
    where
        Self: Sized,
        B: Clone,
    {
        self.fmap(|_| with.clone())
    }
}

pub trait FunctorMut: TypeMatch {
    fn fmap_mut<F, B>(self, mapper: F) -> Reapply<Self, B>
    where
        F: FnMut(Self::Argument) -> B;

    fn replace_mut<B>(self, with: B) -> Reapply<Self, B>
    where
        Self: Sized,
        B: Clone,
    {
        self.fmap_mut(|_| with.clone())
    }
}

impl<T> Functor for T
where
    T: FunctorMut,
{
    fn fmap<F, B>(self, mapper: F) -> Reapply<Self, B>
    where
        F: Fn(Self::Argument) -> B,
    {
        self.fmap_mut(mapper)
    }
}

pub trait FunctorOnce: TypeMatch {
    fn fmap_once<F, B>(self, mapper: F) -> Reapply<Self, B>
    where
        F: FnOnce(Self::Argument) -> B;

    fn replace_once<B>(self, with: B) -> Reapply<Self, B>
    where
        Self: Sized,
    {
        self.fmap_once(|_| with)
    }
}

impl<T> FunctorMut for T
where
    T: FunctorOnce,
{
    fn fmap_mut<F, B>(self, mapper: F) -> Reapply<Self, B>
    where
        F: FnMut(Self::Argument) -> B,
    {
        self.fmap_once(mapper)
    }
}

// IMPLS

impl<T> FunctorOnce for Option<T> {
    fn fmap_once<F, B>(self, mapper: F) -> Reapply<Self, B>
    where
        F: FnOnce(Self::Argument) -> B,
    {
        self.map(mapper)
    }
}

impl<T> FunctorMut for Vec<T> {
    fn fmap_mut<F, B>(self, mapper: F) -> Reapply<Self, B>
    where
        F: FnMut(Self::Argument) -> B,
    {
        self.into_iter().map(mapper).collect()
    }
}

impl<T, E> FunctorOnce for Result<T, E> {
    fn fmap_once<F, B>(self, mapper: F) -> Reapply<Self, B>
    where
        F: FnOnce(Self::Argument) -> B,
    {
        self.map(mapper)
    }
}
